'use strict';

var CustomerService = {};

CustomerService.getCustomers = function getCustomers(connection) {
    var sql = `SELECT id, firstName, lastName, email, gender, birthday FROM customer`;
    return connection.query(sql);
};

CustomerService.getCustomer = function getCategory(connection, id) {
    var sql = `SELECT id, firstName, lastName, email, gender, birthday FROM customer WHERE id = :id`;
    return connection.query(sql, {id : id});
}

CustomerService.createCustomer = function createCustomer(connection, customer) {
    var sql = `INSERT INTO customer (firstName, lastName, email, gender, birthday)
                            VALUES (:firstName, :lastName, :email, :gender, :birthday)`;
    return connection.query(sql, customer);
}

CustomerService.updateCustomer = function updateCustomer(connection, customer) {
    var sql = `UPDATE customer SET
                    firstName = :firstName,
                    lastName = :lastName,
                    email = :email,
                    gender = :gender,
                    birthday = :birthday
                    WHERE id = :id`;
    return connection.query(sql, customer);
}

CustomerService.deleteCustomer = function deleteCustomer(connection, id) {
    var sql = `DELETE FROM customer WHERE id = :id`;
    return connection.query(sql, {id : id});
}

module.exports = CustomerService;