'use strict';

var express = require('express');
var Q = require('q');
var db = require('../database/dbConnection');
var customerService = require('../services/customerService');

var router = express.Router();

router.get('/', function (req, res, next) {
    var connection = null;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return customerService.getCustomers(connection);
    })
    .then(function(data) {
        res.json(data);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.get('/:id', function (req, res, next) {
    var connection = null,
        id = req.params.id;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return customerService.getCustomer(connection, id);
    })
    .then(function(data) {
        res.json(data[0]);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.post('/', function (req, res, next) {
    var connection = null,
        data = null,
        customer = req.body;
    //TODO validate the customer

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return connection.beginTransaction();
    })
    .then(function() {
        return customerService.createCustomer(connection, customer);
    })
    .then(function(result) {
        data = result;
        return connection.commit();
    })
    .then(function () {
        res.json(data)
    })
    .catch(function (err) {
        next(err);
        return connection.rollback();
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.put('/:id', function (req, res, next) {
    var connection = null,
        data = null,        
        id = req.params.id,
        customer = req.body;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return connection.beginTransaction();
    })
    .then(function() {
        return customerService.updateCustomer(connection, customer);
    })
    .then(function(result) {
        data = result;
        return connection.commit();
    })
    .then(function () {
        res.json(data)
    })
    .catch(function (err) {
        next(err);
        return connection.rollback();
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.delete('/:id', function (req, res, next) {
    var connection = null,
        id = req.params.id;
    
    db.getConnection()
    .then(function(conn) {
        connection = conn;
        return customerService.deleteCustomer(connection, id);
    })
    .then(function(result) {
        data = result;
        return connection.commit();
    })
    .then(function () {
        res.json(data)
    })
    .catch(function (err) {
        next(err);
        return connection.rollback();
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

module.exports = router;